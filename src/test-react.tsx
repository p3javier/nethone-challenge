/*
  Below you will find exemplary component that does nothing until window resize.
  On window resize it:
  - asynchronously fetches window dimensions,
  - informs parent about calculated total of even indexed dimensions,
  - shows multiplied entries on the screen.

  Fix errors and improve code quality (remove or add code if needed).
  Use TypeScript.
  entries are null initially until data is fetched.

  React version 17
*/

import * as React from "react";
import MultiList from "./MultiList";

type Props = {
  multiplier: number;
  onNewTotal: (total: number) => void;
};

type State = {
  isVisible: boolean;
  entries: number[] | null;
};

class MultiplierComponent extends React.PureComponent<Props, State> {
  state: State = {
    isVisible: false,
    entries: null,
  };
  static props: Props;

  notify(multiplier = this.props.multiplier) {
    let totalEven: number = 0;
    for (let i = 0; i < this.state.entries?.length!; i++) {
      if (i % 2 === 0) {
        totalEven += this.state.entries![i] * multiplier!;
      }
    }

    this.props.onNewTotal(totalEven);
  }
  componentDidMount() {
    window.addEventListener("resize", this.onResize.bind(this));
  }
  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>) {
    if (prevProps.multiplier !== this.props.multiplier) {
      this.notify(this.props.multiplier);
    }
  }
  static notify(multiplier?: number) {
    throw new Error("Method not implemented.");
  }
  shouldComponentUpdate(nextProps: Props, nextState: State) {
    return (
      nextProps.multiplier !== this.props.multiplier ||
      nextProps.onNewTotal !== this.props.onNewTotal ||
      nextState.entries !== this.state.entries
    );
  }

  onResize() {
    this.setState({ isVisible: true });
    this.fetch();
  }
  async fetch() {
    const entries = await this.loadData();
    this.setState({ entries: entries });
    this.notify();
  }
  loadData(): Promise<number[]> {
    return new Promise((resolve) =>
      window.setTimeout(
        () =>
          resolve([
            window.innerWidth,
            window.innerHeight,
            window.outerWidth,
            window.outerHeight,
            window.screen.width,
          ]),
        1500
      )
    );
  }

  render() {
    const entries = this.state.entries;
    const isVisible = this.state.isVisible;

    if (isVisible === false) {
      return <div>Resize window to see make component visible!</div>;
    }

    return (
      <React.Suspense fallback={<h1>Loading profile...</h1>}>
        Multiplied entries:
        <ul>
          <MultiList entries={entries} multiplier={this.props.multiplier} />
        </ul>
        <span>Window width = {entries![0]}</span>
      </React.Suspense>
    );
  }
}

export default MultiplierComponent;
