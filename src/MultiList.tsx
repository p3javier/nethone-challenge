import * as React from "react";

class MultiList extends React.Component<{
  entries: number[] | null;
  multiplier: number;
}> {
  render() {
    let result = 0;
    let listArray = [];
    for (let i = 0; i < this.props.entries!.length; i++) {
      result = this.props.entries![i] * this.props.multiplier;
      listArray.push(<li>{result}</li>);
    }
    return <>{listArray}</>;
  }
}

export default MultiList;
