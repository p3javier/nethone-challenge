import * as React from "react";

import MultiplierComponent from "./test-react";

class Parent extends React.Component {
  handleTotal(newTotal: number) {
    console.log("newTotal", newTotal);
  }
  render() {
    return <MultiplierComponent multiplier={5} onNewTotal={this.handleTotal} />;
  }
}

export default Parent;
